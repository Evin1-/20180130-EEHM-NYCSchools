package com.example.domain.database;

import com.example.domain.DataSource;
import com.example.domain.dto.School;
import com.example.domain.dto.SchoolDetails;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by evin on 1/30/18.
 */

public class LocalDataSource implements DataSource {

  @Inject
  public LocalDataSource() {

  }

  @Override
  public Single<List<School>> getSchools() {
    return Single.never();
  }

  @Override
  public Single<List<SchoolDetails>> getSchoolScores(String dbn) {
    return Single.never();
  }
}
