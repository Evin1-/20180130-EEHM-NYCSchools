package com.example.domain;

import com.example.domain.dto.School;
import com.example.domain.dto.SchoolDetails;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by evin on 1/30/18.
 */

public interface DataSource {
  Single<List<School>> getSchools();

  Single<List<SchoolDetails>> getSchoolScores(String dbn);
}
