package com.example.domain.api;

import com.example.domain.dto.School;
import com.example.domain.dto.SchoolDetails;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by evin on 1/30/18.
 */

public interface NewYorkService {

  String BASE_URL = "https://data.cityofnewyork.us/";

  @GET("/resource/97mf-9njv.json?boro=Q")
  Single<List<School>> retrieveSchools();

  @GET("/resource/734v-jeq5.json")
  Single<List<SchoolDetails>> retrieveSchoolScores(@Query("dbn") String dbn);

  class Factory {
    public OkHttpClient createOkHttpClient() {
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      return new OkHttpClient.Builder()
          .addInterceptor(interceptor)
          .build();
    }

    public Retrofit createRetrofit(OkHttpClient okHttpClient) {
      return new Retrofit.Builder()
          .baseUrl(BASE_URL)
          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
          .addConverterFactory(GsonConverterFactory.create())
          .client(okHttpClient)
          .build();
    }

    public NewYorkService createNewYorkService(Retrofit retrofit) {
      return retrofit.create(NewYorkService.class);
    }
  }
}
