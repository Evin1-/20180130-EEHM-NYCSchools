package com.example.domain.api;

import com.example.domain.DataSource;
import com.example.domain.dto.School;
import com.example.domain.dto.SchoolDetails;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by evin on 1/30/18.
 */

public class RemoteDataSource implements DataSource {

  private NewYorkService newYorkService;

  @Inject
  RemoteDataSource(NewYorkService newYorkService) {
    this.newYorkService = newYorkService;
  }

  @Override
  public Single<List<School>> getSchools() {
    return newYorkService.retrieveSchools();
  }

  @Override
  public Single<List<SchoolDetails>> getSchoolScores(String dbn) {
    return newYorkService.retrieveSchoolScores(dbn);
  }
}
