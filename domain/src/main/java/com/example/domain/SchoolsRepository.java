package com.example.domain;

import com.example.domain.api.RemoteDataSource;
import com.example.domain.database.LocalDataSource;
import com.example.domain.dto.School;
import com.example.domain.dto.SchoolDetails;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by evin on 1/30/18.
 */

public class SchoolsRepository implements DataSource {

  private RemoteDataSource remoteDataSource;
  private LocalDataSource localDataSource;

  @Inject
  public SchoolsRepository(RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
    this.remoteDataSource = remoteDataSource;
    this.localDataSource = localDataSource;
  }

  // TODO: 1/30/18 Create concatenation from localDataSource single and remoteDataSource
  @Override
  public Single<List<School>> getSchools() {
    return remoteDataSource.getSchools();
  }

  // TODO: 1/30/18 Create concatenation from localDataSource single and remoteDataSource
  @Override
  public Single<List<SchoolDetails>> getSchoolScores(String dbn) {
    return remoteDataSource.getSchoolScores(dbn);
  }
}
