package com.example.nyschools;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.nyschools.ui.main.HomeActivity;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by evin on 2/1/18.
 */

@RunWith(AndroidJUnit4.class)
public class NormalFlowInstrumentedTest {
  @Rule
  public ActivityTestRule<HomeActivity> homeActivityActivityTestRule =
      new ActivityTestRule<>(HomeActivity.class);

  @SuppressWarnings("deprecation")
  @Before
  public void setUp() throws Exception {
    CountingIdlingResource[] homeActivityIdlingResources = homeActivityActivityTestRule.getActivity()
        .getIdlingResources();
    Espresso.registerIdlingResources(homeActivityIdlingResources);
  }

  @Test
  public void onAppOpening_shouldShowList() {
    getViewFromId(R.id.a_main_recycler)
        .perform(RecyclerViewActions.scrollToPosition(10));

    getViewFromId(R.id.a_main_recycler)
        .perform(RecyclerViewActions.actionOnItemAtPosition(10, click()));
  }

  private ViewInteraction getViewFromId(int id) {
    return onView(withId(id));
  }
}
