package com.example.nyschools.ui.main;

import com.example.domain.SchoolsRepository;
import com.example.domain.dto.School;
import com.example.nyschools.ui.details.DetailsContract;
import com.example.nyschools.ui.details.DetailsPresenter;
import com.example.nyschools.util.rx.SchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by evin on 1/30/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

  @Mock
  SchoolsRepository schoolsRepository;

  @Mock
  HomeContract.View view;

  private HomePresenter homePresenter;

  @Before
  public void setUp() throws Exception {
    homePresenter = new HomePresenter(schoolsRepository, new SchedulerProvider() {
      @Override
      public Scheduler ui() {
        return Schedulers.trampoline();
      }

      @Override
      public Scheduler computation() {
        return null;
      }

      @Override
      public Scheduler io() {
        return Schedulers.trampoline();
      }
    });
  }

  @After
  public void tearDown() throws Exception {
    homePresenter = null;
  }

  @Test
  public void loadData() throws Exception {
    when(schoolsRepository.getSchools()).thenReturn(Single.just(new ArrayList<>()));

    homePresenter.attachView(view);
    homePresenter.loadData();

    verify(view).showProgress();
    verify(view).hideProgress();
    verify(view).showData(anyListOf(School.class));
  }
}