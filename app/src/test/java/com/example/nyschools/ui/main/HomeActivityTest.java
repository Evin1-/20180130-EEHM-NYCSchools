package com.example.nyschools.ui.main;

import android.view.View;
import android.widget.ProgressBar;

import com.example.nyschools.BuildConfig;
import com.example.nyschools.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

/**
 * Created by evin on 2/1/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class HomeActivityTest {

  private HomeActivity homeActivity;

  @Before
  public void setUp() throws Exception {
    homeActivity = Robolectric.setupActivity(HomeActivity.class);
  }

  @After
  public void tearDown() throws Exception {
    homeActivity = null;
  }

  @Test
  public void onShowProgress_shouldSeeProgressBar() throws Exception {
    ProgressBar progressBar = homeActivity.findViewById(R.id.a_main_progress);

    homeActivity.showProgress();

    assertThat(progressBar.getVisibility(), equalTo(View.VISIBLE));
  }

  @Test
  public void onHideProgress_shouldNotSeeProgressBar() throws Exception {
    ProgressBar progressBar = homeActivity.findViewById(R.id.a_main_progress);

    homeActivity.showProgress();
    homeActivity.hideProgress();

    assertThat(progressBar.getVisibility(), equalTo(View.INVISIBLE));
  }

  @Test
  public void onShowError_shouldShowToast() throws Exception {
    String error = "Something went wrong!";

    homeActivity.showError(error);

    assertThat(ShadowToast.getTextOfLatestToast(), equalTo(error));
  }
}