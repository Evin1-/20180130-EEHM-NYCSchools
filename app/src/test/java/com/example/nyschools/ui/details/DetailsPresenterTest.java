package com.example.nyschools.ui.details;

import com.example.domain.SchoolsRepository;
import com.example.nyschools.util.rx.SchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by evin on 1/30/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class DetailsPresenterTest {

  @Mock
  SchoolsRepository schoolsRepository;

  @Mock
  DetailsContract.View view;

  DetailsPresenter detailsPresenter;

  @Before
  public void setUp() throws Exception {
    detailsPresenter = new DetailsPresenter(schoolsRepository, new SchedulerProvider() {
      @Override
      public Scheduler ui() {
        return Schedulers.trampoline();
      }

      @Override
      public Scheduler computation() {
        return null;
      }

      @Override
      public Scheduler io() {
        return Schedulers.trampoline();
      }
    });
  }

  @After
  public void tearDown() throws Exception {
    detailsPresenter = null;
  }

  @Test
  public void onEmptyLoadData_shouldCallDataNotAvailable() throws Exception {
    String fakeDbn = "01M509";
    when(schoolsRepository.getSchoolScores(fakeDbn)).thenReturn(Single.just(new ArrayList<>()));

    detailsPresenter.attachView(view);
    detailsPresenter.loadData(fakeDbn);

    verify(view).dataNotAvailable();
  }
}