package com.example.nyschools.ui.main;

import android.os.Bundle;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.domain.dto.School;
import com.example.nyschools.R;
import com.example.nyschools.SchoolsApp;
import com.example.nyschools.di.MainComponent;
import com.example.nyschools.ui.details.DetailsActivity;
import com.example.nyschools.ui.main.adapter.HomeAdapter;
import com.example.nyschools.ui.main.di.DaggerHomeComponent;
import com.example.nyschools.util.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements HomeContract.View {

  private static final String TAG = "HomeActivityTAG_";

  private HomeAdapter homeAdapter;
  private CountingIdlingResource idlingResource =
      new CountingIdlingResource("NETWORK_CALL");

  @BindView(R.id.a_main_recycler)
  RecyclerView recyclerView;

  @BindView(R.id.a_main_progress)
  ProgressBar progressBar;

  @Inject
  HomePresenter homePresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);

    injectDependencies();
    initViews();

    homePresenter.attachView(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    homePresenter.detachView();
  }

  @Override
  protected void onStart() {
    super.onStart();

    homePresenter.loadData();
  }

  @Override
  public void showData(List<School> schools) {
    homeAdapter.refreshData(schools);
    idlingResource.decrement();
  }

  @Override
  public void showProgress() {
    idlingResource.increment();
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgress() {
    progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  public void showError(String error) {
    Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    idlingResource.decrement();
  }

  @Override
  public void injectDependencies() {
    MainComponent mainComponent = ((SchoolsApp) getApplication()).getMainComponent();

    DaggerHomeComponent.builder()
        .mainComponent(mainComponent)
        .build()
        .inject(this);
  }

  private void initViews() {
    ButterKnife.bind(this);

    homeAdapter = new HomeAdapter(new ArrayList<>(), this::navigateDetails);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(homeAdapter);
    recyclerView.setHasFixedSize(true);
    recyclerView.addItemDecoration(new SpacesItemDecoration(10));
  }

  private void navigateDetails(School school) {
    DetailsActivity.start(this, school);
  }

  /* *
  * Espresso helper to get all of the IdlingResources related to this Activity
  */
  public CountingIdlingResource[] getIdlingResources() {
    return new CountingIdlingResource[]{idlingResource};
  }

  public interface SchoolClickListener {
    void onSchoolSelected(School school);
  }
}
