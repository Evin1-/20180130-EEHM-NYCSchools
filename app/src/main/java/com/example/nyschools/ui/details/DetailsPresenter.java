package com.example.nyschools.ui.details;

import com.example.domain.SchoolsRepository;
import com.example.nyschools.util.rx.SchedulerProvider;

import javax.inject.Inject;

/**
 * Created by evin on 1/30/18.
 */

public class DetailsPresenter implements DetailsContract.Presenter {

  private DetailsContract.View view;
  private SchoolsRepository schoolsRepository;
  private SchedulerProvider schedulerProvider;

  @Inject
  public DetailsPresenter(SchoolsRepository schoolsRepository, SchedulerProvider schedulerProvider) {
    this.schoolsRepository = schoolsRepository;
    this.schedulerProvider = schedulerProvider;
  }

  @Override
  public void attachView(DetailsContract.View view) {
    this.view = view;
  }

  @Override
  public void detachView() {
    this.view = null;
  }

  @Override
  public void loadData(String dbn) {
    view.showProgress();

    schoolsRepository.getSchoolScores(dbn)
        .map(list -> list.isEmpty() ? null : list.get(0))
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .subscribe(schoolDetails -> {
          view.showData(schoolDetails);
          view.hideProgress();
        }, throwable -> {
          view.dataNotAvailable();
          view.hideProgress();
        });
  }
}
