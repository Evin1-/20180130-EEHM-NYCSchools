package com.example.nyschools.ui.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.domain.dto.School;
import com.example.nyschools.R;
import com.example.nyschools.ui.main.HomeActivity;

import java.util.List;

/**
 * Created by evin on 1/30/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

  private List<School> schools;
  private HomeActivity.SchoolClickListener schoolClickListener;

  public HomeAdapter(List<School> schools, HomeActivity.SchoolClickListener schoolClickListener) {
    this.schools = schools;
    this.schoolClickListener = schoolClickListener;
  }

  @Override
  public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.recycler_school, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {
    holder.bind(schools.get(position));
  }

  @Override
  public int getItemCount() {
    return schools == null
        ? 0
        : schools.size();
  }

  public void refreshData(List<School> schools) {
    this.schools.clear();
    this.schools.addAll(schools);
    notifyDataSetChanged();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView nameTextView;
    private final TextView addressTextView;
    private final TextView phoneTextView;
    private final Button button;

    private ViewHolder(View itemView) {
      super(itemView);

      nameTextView = itemView.findViewById(R.id.r_school_name);
      addressTextView = itemView.findViewById(R.id.r_school_address);
      phoneTextView = itemView.findViewById(R.id.r_school_phone);
      button = itemView.findViewById(R.id.r_school_button);
    }

    private void bind(School school) {
      nameTextView.setText(school.getSchoolName());
      addressTextView.setText(school.getPrimaryAddressLine1());
      phoneTextView.setText(school.getPhoneNumber());

      button.setOnClickListener(v -> schoolClickListener.onSchoolSelected(school));
    }
  }
}
