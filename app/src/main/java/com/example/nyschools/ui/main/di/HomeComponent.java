package com.example.nyschools.ui.main.di;

import com.example.nyschools.di.MainComponent;
import com.example.nyschools.di.scopes.PerView;
import com.example.nyschools.ui.main.HomeActivity;

import dagger.Component;

/**
 * Created by evin on 1/30/18.
 */

@PerView
@Component(dependencies = MainComponent.class)
public interface HomeComponent {
  void inject(HomeActivity homeActivity);
}
