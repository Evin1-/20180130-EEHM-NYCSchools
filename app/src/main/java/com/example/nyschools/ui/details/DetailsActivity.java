package com.example.nyschools.ui.details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.domain.dto.School;
import com.example.domain.dto.SchoolDetails;
import com.example.nyschools.R;
import com.example.nyschools.SchoolsApp;
import com.example.nyschools.di.MainComponent;
import com.example.nyschools.ui.details.di.DaggerDetailsComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by evin on 1/30/18.
 */

public class DetailsActivity extends AppCompatActivity implements DetailsContract.View {

  private static final String KEY_SCHOOL_BUNDLE = "KEY_SCHOOL_BUNDLE";

  @BindView(R.id.a_details_progress)
  ProgressBar progressBar;
  @BindView(R.id.a_details_math)
  TextView mathTextView;
  @BindView(R.id.a_details_reading)
  TextView readingTextView;
  @BindView(R.id.a_details_writing)
  TextView writingTextView;

  @Inject
  DetailsPresenter detailsPresenter;

  public static void start(Context context, School school) {
    Intent starter = new Intent(context, DetailsActivity.class);
    starter.putExtra(KEY_SCHOOL_BUNDLE, school);
    context.startActivity(starter);
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_details);

    injectDependencies();

    detailsPresenter.attachView(this);

    ButterKnife.bind(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    detailsPresenter.detachView();
  }

  @Override
  protected void onStart() {
    super.onStart();

    School school = parseSchoolFromArguments();
    detailsPresenter.loadData(school.getDbn());

    setupToolbar(school);
  }

  @Override
  public void showData(SchoolDetails schoolDetails) {
    mathTextView.setText(schoolDetails.getSatMathAvgScore());
    readingTextView.setText(schoolDetails.getSatCriticalReadingAvgScore());
    writingTextView.setText(schoolDetails.getSatWritingAvgScore());
  }

  @Override
  public void dataNotAvailable() {
    mathTextView.setText(getString(R.string.not_available));
    readingTextView.setText(getString(R.string.not_available));
    writingTextView.setText(getString(R.string.not_available));
  }

  @Override
  public void showProgress() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgress() {
    progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  public void showError(String error) {
    Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void injectDependencies() {
    MainComponent mainComponent = ((SchoolsApp) getApplication()).getMainComponent();

    DaggerDetailsComponent.builder()
        .mainComponent(mainComponent)
        .build()
        .inject(this);
  }

  private void setupToolbar(School school) {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setTitle(school.getSchoolName());
      actionBar.setDisplayHomeAsUpEnabled(true);
      actionBar.setDisplayShowHomeEnabled(true);
    }
  }

  private School parseSchoolFromArguments() {
    Intent intent = getIntent();
    return (School) intent.getSerializableExtra(KEY_SCHOOL_BUNDLE);
  }
}
