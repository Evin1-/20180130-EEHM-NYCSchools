package com.example.nyschools.ui.main;

import com.example.domain.dto.School;

import java.util.List;

/**
 * Created by evin on 1/30/18.
 */

public interface HomeContract {
  interface View {
    void showData(List<School> schools);

    void showProgress();

    void hideProgress();

    void showError(String error);

    void injectDependencies();
  }

  interface Presenter {
    void attachView(View view);

    void detachView();

    void loadData();
  }
}
