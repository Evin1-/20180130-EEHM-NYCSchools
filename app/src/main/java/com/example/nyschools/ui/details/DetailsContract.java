package com.example.nyschools.ui.details;

import com.example.domain.dto.SchoolDetails;

/**
 * Created by evin on 1/30/18.
 */

public interface DetailsContract {
  interface View {
    void showData(SchoolDetails schoolDetails);

    void dataNotAvailable();

    void showProgress();

    void hideProgress();

    void showError(String error);

    void injectDependencies();
  }

  interface Presenter {
    void attachView(View view);

    void detachView();

    void loadData(String dbn);
  }
}
