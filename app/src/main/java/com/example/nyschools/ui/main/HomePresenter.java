package com.example.nyschools.ui.main;

import com.example.domain.SchoolsRepository;
import com.example.nyschools.util.rx.SchedulerProvider;

import javax.inject.Inject;

/**
 * Created by evin on 1/30/18.
 */

public class HomePresenter implements HomeContract.Presenter {

  private HomeContract.View view;
  private SchoolsRepository schoolsRepository;
  private SchedulerProvider schedulerProvider;

  @Inject
  public HomePresenter(SchoolsRepository schoolsRepository, SchedulerProvider schedulerProvider) {
    this.schoolsRepository = schoolsRepository;
    this.schedulerProvider = schedulerProvider;
  }

  @Override
  public void attachView(HomeContract.View view) {
    this.view = view;
  }

  @Override
  public void detachView() {
    this.view = null;
  }

  @Override
  public void loadData() {
    view.showProgress();

    schoolsRepository.getSchools()
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .subscribe(schools -> {
          view.showData(schools);
          view.hideProgress();
        }, throwable -> {
          view.showError(throwable.getLocalizedMessage());
          view.hideProgress();
        });
  }
}
