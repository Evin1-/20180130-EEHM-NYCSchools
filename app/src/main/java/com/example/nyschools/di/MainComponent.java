package com.example.nyschools.di;

import com.example.domain.SchoolsRepository;
import com.example.nyschools.util.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by evin on 1/30/18.
 */

@Singleton
@Component(modules = MainModule.class)
public interface MainComponent {
  SchoolsRepository schoolsRepository();

  SchedulerProvider schedulerProvider();
}
