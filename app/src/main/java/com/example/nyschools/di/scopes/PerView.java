package com.example.nyschools.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by evin on 1/30/18.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerView {

}
