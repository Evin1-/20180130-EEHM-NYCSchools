package com.example.nyschools.di;

import com.example.domain.api.NewYorkService;
import com.example.nyschools.util.rx.AppSchedulerProvider;
import com.example.nyschools.util.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by evin on 1/30/18.
 */

@Module
public class MainModule {

  @Singleton
  @Provides
  OkHttpClient provideOkHttpClient() {
    return new NewYorkService.Factory().createOkHttpClient();
  }

  @Singleton
  @Provides
  Retrofit provideRetrofit(OkHttpClient okHttpClient) {
    return new NewYorkService.Factory().createRetrofit(okHttpClient);
  }

  @Singleton
  @Provides
  NewYorkService provideNewYorkService(Retrofit retrofit) {
    return new NewYorkService.Factory().createNewYorkService(retrofit);
  }

  @Singleton
  @Provides
  SchedulerProvider provideSchedulerProvider() {
    return new AppSchedulerProvider();
  }
}
