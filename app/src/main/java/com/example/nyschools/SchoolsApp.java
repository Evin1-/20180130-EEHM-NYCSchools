package com.example.nyschools;

import android.app.Application;

import com.example.nyschools.di.DaggerMainComponent;
import com.example.nyschools.di.MainComponent;

/**
 * Created by evin on 1/30/18.
 */

public class SchoolsApp extends Application {

  private MainComponent mainComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    mainComponent = DaggerMainComponent.create();
  }

  public MainComponent getMainComponent() {
    return mainComponent;
  }
}
